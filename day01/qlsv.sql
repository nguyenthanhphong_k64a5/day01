CREATE DATABASE QLSV;

CREATE TABLE QLSV.DMKHOA (
	MaKH varchar(6) NOT null,
    TenKhoa varchar(30) not null,
    PRIMARY KEY (MaKH)
);

CREATE TABLE QLSV.SINHVIEN (
	MaSV varchar(6) NOT null,
    HoSV varchar(30) NOT null,
    TenSV varchar(15) NOT null,
    GioiTinh char(1) NOT null,
    NgaySinh datetime,
    NoiSinh varchar(50) NOT null,
    DiaChi varchar(50) NOT null,
    MaKH varchar(6) NOT null,
    HocBong int,
    PRIMARY KEY(MaSV)
);
