<?php
$gender = array(0 => 'Nam', 1=> 'Nữ');
?>
<?php
    $error = "";
    session_start();
    $data = array();

    if ($_SERVER["REQUEST_METHOD"] == "POST"){

        if (!isset($_POST['name']) || $_POST['name'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy nhập họ tên.</font></p>";
        }
        
        if (!isset($_POST['gender']) || $_POST['gender'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy chọn giới tính.</font></p>";
        }

        if(isset($_POST['name']) && isset($_POST['gender'])){
            header("location: registstudent.php"); 
        }

        $_SESSION = $_POST;
    }
    $_SESSION['gender_array'] = $gender;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <script>
        function updateDistricts() {
            var citySelect = document.getElementById('city');
            var districtSelect = document.getElementById('district');

            // Clear the current options in the "Quận" select box
            districtSelect.innerHTML = '<option value="">Chọn Quận</option>';

            var selectedCity = citySelect.value;
            if (selectedCity === 'Hanoi') {
                var hanoiDistricts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                hanoiDistricts.forEach(function (district) {
                    var option = document.createElement('option');
                    option.value = district;
                    option.textContent = district;
                    districtSelect.appendChild(option);
                });
            } else if (selectedCity === 'HCMC') {
                var hcmcDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                hcmcDistricts.forEach(function (district) {
                    var option = document.createElement('option');
                    option.value = district;
                    option.textContent = district;
                    districtSelect.appendChild(option);
                });
            }
        }

        function validateForm() {
            var fullName = document.getElementById('full_name').value;
            var gender = document.querySelector('input[name="gender"]:checked');
            var birthYear = document.getElementById('birth_year').value;
            var city = document.getElementById('city').value;
            var district = document.getElementById('district').value;
            var otherInfo = document.getElementById('other_info').value;

            if (fullName === '' || !gender || birthYear === '' || city === '' || district === '' || otherInfo === '') {
                alert('Vui lòng điền đầy đủ thông tin.');
                return false;
            }
            return true;
        }
    </script>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
body {
    display: flex;
    justify-content: center;
}

.wrapper {
    border: 1px solid #41719c;
    padding-top: 40px;
    padding-right: 100px;
    padding-bottom: 15px;
    padding-left: 25px;
    display: inline-block;
}

.required:after {
    content:"*";
    color: red;
}

label {
    background-color: #70AD47;
    color: white;
    width: 80px;
    padding-left: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 10px;
    border: 1px solid #41719c;
    margin-bottom: 10px;
    display: inline-block;
}

input[type=text2] {
    width: 300px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

select{
    width: 150px;
    height: 40px;
    line-height: 50px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=text1]{
    width: 144px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=radio]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #70AD47;
}

input[type=file]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
}

input[type = submit] {
    width: 120px;
    height: 45px;
    background-color: #70AD47;
    color: white;
    margin-left: 190px;
    border: 1px solid #41719c;
    padding: 5px 25px;
    border-radius: 7px;
    margin-top: 15px;
}

.image_input {
    display: flex;
    align-items: center;
}

</style>
<body>
    <script type="text/javascript">
        function readURL(input) {
            console.log(input.files[0]);
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    $('#blah').css("display","block");
                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result);
                        
                            $('.img_encode').val(e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }else{
                    $('#blah').css("display","none");
                }
            }
    </script>
    <div class = "wrapper">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="error">
            <?php
            echo $error
            ?>
        </div>
        <div class = "name">
            <label class = "required">Họ và tên</label>
            <input type="text2" name = "name">
        </div>

        <div class = "Gender">
            <label class = "required">Giới tính</label>
            <?php 
                $gender = array(0 => 'Nam', 1=> 'Nữ');
                for ($i = 0; $i < count($gender); $i++){
                    echo "<input type=\"radio\" name = \"gender\"  value=\"$i\">{$gender[$i]}";
                }
            ?>
        </div>

        <div class = "birthday">
            <label class = "required">Ngày sinh</label>
            <select name = "year">
                <option>Year</option>
                <?php
                    $y = date("Y", strtotime("+8 HOURS"));
                    for($year = 2008; $year >= 1983; $year--){
                        echo "<option value = '".$year."'>".$year."</option>";
                    }
                ?>
            </select>
            <select name = "month">
                <option>Month</option>
                <?php
                    for($month = 1; $month <= 12; $month++)
                    echo"<option value = '".$month."'>".$month."</option>";
                ?>
            </select>
            <select name = "day">
                <option>Day</option>
                <?php
                    for($day = 1; $day <= 31; $day++){
                    echo "<option value = '".$day."'>".$day."</option>";
                }
                ?>
            </select>
        </div>

        <div class = "Address">
            <label for="city">Thành phố:</label>
            <select name="city" id="city" onchange="updateDistricts()">
                <option value="">Chọn Thành phố</option>
                <option value="Hanoi">Hà Nội</option>
                <option value="HCMC">Tp.Hồ Chí Minh</option>
            </select><br><br>

            <label for="district">Quận:</label>
            <select name="district" id="district">
                <option value="">Chọn Quận</option>
            </select><br><br>

        </div>

        <div class = "info">
            <label >Thông tin khác</label>
            <input type="text2" name = "info">
        </div>

        <input type="submit" value="Đăng ký"/></td>
    </form>
    </div>
</body>

<?php
?>
</html>